#!/usr/bin/env bash

if [ -z "$*" ]
then
	echo "Please provide the url the flask server will run on."
       	exit 1
fi

echo "Setting up webhook for server hosted at '$1'"

 curl -s \            
 	-X POST \
 	-H "Content-Type:application/json" \
 	-H "x-api-key: $HABITICA_TOKEN" \
 	-H "x-api-user: $HABITICA_UUID" \
 	https://habitica.com/api/v3/user/webhook \
 	-d '{
 		"enabled": true,
 		"url": "$1",
 		"label": "Habitica to Telegram bridge",
 		"type": "groupChatReceived",
 		"options": {
 		"groupId": "870b8109-badb-47b6-9e71-7f20948babe4"
 	}}'


curl -s \            
	-X POST \
	-H "Content-Type:application/json" \
	-H "x-api-key: $HABITICA_TOKEN" \
	-H "x-api-user: $HABITICA_UUID" \
	https://habitica.com/api/v3/user/webhook \
	-d '{
		"enabled": true,
		"url": "$1",
		"label": "Habitica to Telegram bridge",
		"type": "questActivity",
		}'

